package eu.future.earth.postcodeapi.client;

import java.io.Serializable;

/**
 * Holder to allow handling of failed searches and missing api keys.
 */
public class PostalCodeSearchResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6504455098124295273L;

	private Address found = null;

	private PostalCodeSearchStatus status = PostalCodeSearchStatus.Succces;

	public Address getFound() {
		return found;
	}

	public void setFound(Address found) {
		this.found = found;
	}

	public PostalCodeSearchStatus getStatus() {
		return status;
	}

	public void setStatus(PostalCodeSearchStatus status) {
		this.status = status;
	}

}
