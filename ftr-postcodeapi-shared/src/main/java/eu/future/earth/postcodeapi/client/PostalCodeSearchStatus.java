package eu.future.earth.postcodeapi.client;

public enum PostalCodeSearchStatus {

	Succces, ApiKeyMissingOrInvalid, AllCallsUsed, UnknownError
	
}
