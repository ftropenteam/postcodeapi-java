package eu.future.earth.postcodeapi.server;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import eu.future.earth.postcodeapi.client.Address;
import eu.future.earth.postcodeapi.client.PostalCodeSearchResult;

public class ServerSideClientTest {



	public static void main(String[] args) {
		ServerSideClient run = new ServerSideClient();
		String currentPostalCode = "3894AB";
		String currentHouseNumber = "4";
		run.setApiKey("insertapikey");
		PostalCodeSearchResult result = run.searchAdressBasedOnPostalCode(currentPostalCode, currentHouseNumber);
		System.out.println("Should be Zeewolde and is :" + result.getFound().getCity().getLabel());
		System.out.println("Should be 5.5268347 and is"
				+ result.getFound().getGeo().getCenter().getWgs84().getCoordinates().get(0));
		System.out.println("Should be Erenstein and is "
				+ result.getFound().getStreet());
		
	}

	@Test
	public void testVestaAxInschieten() throws IOException {
		String json = IOUtils.toString(ServerSideClientTest.class.getResource("/postalcode.json"));
		List<Address> name = ServerSideClient.extractPostalCodes(json);
		assertEquals("Zeewolde", name.get(0).getCity().getLabel());
		assertEquals(new Double(5.5268347), name.get(0).getGeo().getCenter().getWgs84().getCoordinates().get(0));
	}

}
