package eu.future.earth.postcodeapi.server;

import eu.future.earth.postcodeapi.client.Address;
import eu.future.earth.postcodeapi.client.PostalCodeSearchResult;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RunServerSideClient {



	public static void main(String[] args) {
		ServerSideClient run = new ServerSideClient();
		String currentPostalCode = "6921DM";
		String currentHouseNumber = "6921 D";
		run.setApiKey("nld5eO72fG67HJ7OwQRRB7bOthdYiKMzarUcCvZL");
		PostalCodeSearchResult result = run.searchAdressBasedOnPostalCode(currentPostalCode, currentHouseNumber);
		System.out.println("Should be Zeewolde and is :" + result.getFound().getCity().getLabel());
		System.out.println("Should be 5.5268347 and is"
				+ result.getFound().getGeo().getCenter().getWgs84().getCoordinates().get(0));
		System.out.println("Should be Erenstein and is "
				+ result.getFound().getStreet());
		
	}


}
