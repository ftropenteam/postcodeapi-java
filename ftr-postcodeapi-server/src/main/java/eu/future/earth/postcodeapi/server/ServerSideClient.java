package eu.future.earth.postcodeapi.server;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.future.earth.postcodeapi.client.Address;
import eu.future.earth.postcodeapi.client.Center;
import eu.future.earth.postcodeapi.client.City;
import eu.future.earth.postcodeapi.client.Crs;
import eu.future.earth.postcodeapi.client.Geo;
import eu.future.earth.postcodeapi.client.PostalCodeSearchResult;
import eu.future.earth.postcodeapi.client.PostalCodeSearchStatus;
import eu.future.earth.postcodeapi.client.Properties;
import eu.future.earth.postcodeapi.client.Rd;
import eu.future.earth.postcodeapi.client.Wgs84;

/**
 * This is the client class that runs the lookup.
 *
 */
public class ServerSideClient {
	private static final Logger LOGGER = LoggerFactory.getLogger(ServerSideClient.class);

	private String apiKey = null;

	/**
	 * Default contructor.
	 */
	public ServerSideClient() {
		super();
	}

	public PostalCodeSearchResult searchAdressBasedOnPostalCode(final String currentPostalCode, final String currentHouseNumber) {
		final PostalCodeSearchResult result = new PostalCodeSearchResult();
		try {
			URIBuilder builder = new URIBuilder("https://postcode-api.apiwise.nl/v2/addresses/");
			builder.setParameter("postcode", currentPostalCode).setParameter("action", currentHouseNumber);
			final HttpGet request = new HttpGet(builder.build());
			request.addHeader("accept", "application/json");
			request.addHeader("X-Api-Key", apiKey);
			final HttpClient client = HttpClientBuilder.create().build();

			final HttpResponse response = client.execute(request);
			final int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_OK) {
				final String contect = IOUtils.toString(response.getEntity().getContent());
				final List<Address> all = extractPostalCodes(contect);
				if (!all.isEmpty()) {
					result.setFound(all.get(0));
				}
			} else {
				if (statusCode == HttpStatus.SC_UNAUTHORIZED) {
					result.setStatus(PostalCodeSearchStatus.ApiKeyMissingOrInvalid);
				} else {
					if (statusCode == 429) {
						result.setStatus(PostalCodeSearchStatus.AllCallsUsed);
					} else {
						result.setStatus(PostalCodeSearchStatus.UnknownError);
					}
				}
			}
		} catch (final IOException e) {
			result.setStatus(PostalCodeSearchStatus.UnknownError);
			LOGGER.error("Error performing Rest call to PostalCode API.", e);
		} catch (URISyntaxException e) {
			result.setStatus(PostalCodeSearchStatus.UnknownError);
			LOGGER.error("Error performing Rest call to PostalCode API.", e);
		}
		return result;
	}

	public static List<Address> extractPostalCodes(final String json) {
		final List<Address> result = new ArrayList<Address>();
		final JSONObject val = new JSONObject(json);
		final JSONObject embedded = val.getJSONObject("_embedded");
		final JSONArray addresses = embedded.getJSONArray("addresses");
		for (int i = 0; i < addresses.length(); i++) {
			final JSONObject adress = addresses.getJSONObject(i);
			final Address toAdd = new Address();
			final JSONObject cityJson = adress.getJSONObject("city");
			final City city = new City();
			city.setId(cityJson.getString("id"));
			city.setLabel(cityJson.getString("label"));
			toAdd.setCity(city);
			result.add(toAdd);
			if (!adress.isNull("addition")) {
				toAdd.setAddition(adress.getString("addition"));
			}
			toAdd.setStreet(adress.getString("street"));
			if (!adress.isNull("letter")) {
				toAdd.setLetter(adress.getString("letter"));
			}
			toAdd.setId(adress.getString("id"));
			toAdd.setPostcode(adress.getString("postcode"));
			toAdd.setPurpose(adress.getString("purpose"));
			toAdd.setYear(adress.getInt("year"));
			toAdd.setStreet(adress.getString("street"));
			toAdd.setNumber(adress.getInt("number"));
			toAdd.setType(adress.getString("type"));

			final JSONObject geoJson = adress.getJSONObject("geo");
			if (geoJson != null) {
				final Geo geo = new Geo();
				toAdd.setGeo(geo);

				if (geoJson.has("center")) {
					final JSONObject centerJson = geoJson.getJSONObject("center");
					final Center center = new Center();
					geo.setCenter(center);

					if (centerJson.has("wgs84")) {
						final JSONObject wgs84Json = centerJson.getJSONObject("wgs84");
						final Wgs84 wgs84 = new Wgs84();
						center.setWgs84(wgs84);
						wgs84.setType(wgs84Json.getString("type"));
						final JSONArray coordinatesJson = wgs84Json.getJSONArray("coordinates");
						for (int c = 0; c < coordinatesJson.length(); c++) {
							wgs84.getCoordinates().add(coordinatesJson.getDouble(c));
						}
						wgs84.setCrs(extractCoordinates(wgs84Json.getJSONObject("crs")));
					}
					if (centerJson.has("rd")) {
						final JSONObject rdJson = centerJson.getJSONObject("rd");
						final Rd rd = new Rd();
						center.setRd(rd);
						rd.setType(rdJson.getString("type"));
						final JSONArray coordinatesJson = rdJson.getJSONArray("coordinates");
						for (int c = 0; c < coordinatesJson.length(); c++) {
							rd.getCoordinates().add(coordinatesJson.getDouble(c));
						}
						rd.setCrs(extractCoordinates(rdJson.getJSONObject("crs")));
					}
				}

			}
		}

		return result;
	}

	private static Crs extractCoordinates(final JSONObject jsonObject) {
		final Crs result = new Crs();
		final Properties properties = new Properties();
		final JSONObject propertiesJson = jsonObject.getJSONObject("properties");
		properties.setName(propertiesJson.getString("name"));
		result.setProperties(properties);
		result.setType(jsonObject.getString("type"));
		return result;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(final String apiKey) {
		this.apiKey = apiKey;
	}

}
